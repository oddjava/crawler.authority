package authorityCrawler;

import java.io.IOException;
import java.util.HashMap;

import javax.xml.ws.http.HTTPException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class KeywordAutomation {

	public static void main(String[] args) throws Exception {
		Node node1 = new Node();
		KeywordAutomation keyauto = new KeywordAutomation();
		String url = "https://blog.shareaholic.com/hard-marketing-truths/";
		try {
			node1 = keyauto.chechMetaData(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Node:::" + node1);
	}

	// This method will take input as url and it will return Node object
	// in Node object it will give us
	// keywordTag,descriptionTag,
	// keywordContent,descriptionContents

	public Node chechMetaData(String url) {
		String descriptionTag = "", keywordTag = "";
		Node node = new Node();
		String keywordContent = "", descriptionContents = "";
		try {

			Document doc = Jsoup.connect(url).timeout(1000000).get();
			node.title = doc.title();
			// System.out.println(doc);
			for (Element meta : doc.select("meta")) {
				if (meta.attr("name").toLowerCase().contains("keyword")) {
					// System.out.println("true");
					keywordTag = meta.attr("name");
					node.keywordTag.add(keywordTag);
					keywordContent = meta.attr("content");
					keywordContent = keywordContent.trim();
					if(keywordContent.isEmpty() || keywordContent.equals(""))
					{
						keywordContent = doc.text();
					}
					node.keywordContent = node.keywordContent + "," + keywordContent;
					node.keywordContent = node.keywordContent.substring(1);
				}

				if (meta.attr("name").toLowerCase().contains("description")) {
					descriptionTag = meta.attr("name");
					node.descriptionTag.add(descriptionTag);
					descriptionContents = meta.attr("content");
					descriptionContents = descriptionContents.trim();
					if(descriptionContents.isEmpty() || descriptionContents.equals(""))
					{
						descriptionContents = doc.text();
					}

					node.descriptionContents = node.descriptionContents + "\t" + descriptionContents;

				}

			}
		} catch (IOException e) {
			System.out.println("IO Exception for "+url);
		}
		catch (HTTPException e) {
			System.out.println("HTTP Status exception for "+url);
		}

		return node;
	}

}