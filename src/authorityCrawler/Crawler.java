package authorityCrawler;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jsoup.nodes.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import mongoDBConnection.GetConnection;
import removeSpecialChar.RemoveSpecialChar;

public class Crawler {

	private static final int MYTHREADS = 50;
	static int count = 1;
	static DBCollection crawldata = GetConnection.connect("crawldata");
	static DBCollection samplecrawl = GetConnection.connect("sampleCrawldata");
	static DBCollection domainDB = GetConnection.connect("domain");
	static DBCollection settingsDB = GetConnection.connect("settings");
	static DBCollection dataCountDB = GetConnection.connect("dataCount");

	static localData data;
	static BasicDBObject insertObject = new BasicDBObject();

	public void crawlSeeds(ArrayList<localData> list) {
		ExecutorService executor = Executors.newFixedThreadPool(MYTHREADS);
		for (int i = 0; i < list.size(); i++) {
			System.out.println("Seed is " + list.get(i).url);

			String url = list.get(i).url;
			boolean status = list.get(i).status;
			String systemdate = list.get(i).systemDate;
			String subCategory = list.get(i).subCategory;
			String moduleName = list.get(i).moduleName;
			int id = list.get(i).id;
			Runnable worker = new MyRunnable(id, url, status, subCategory, systemdate, moduleName);
			// executor.submit(worker);
			executor.execute(worker);
		}
		executor.shutdown();
		// Wait until all threads are finish
		while (!executor.isTerminated()) {

		}
		System.out.println("\nFinished all threads");

	}

	public static void main(String args[]) throws Exception {

		String subCategory, systemDate, moduleName;
		DBCursor cursor;
		int cursorSize;
		DBObject object;
		long startTime = System.currentTimeMillis();
		int cursorLimit = 0;
		int batch = 0;
		String link, authorityStatus, nerStatus, imageStatus, translationStatus;
		boolean status = false;
		int id;

		ArrayList<localData> list = new ArrayList<>();
		ArrayList<localData> crawlList = new ArrayList<>();

		BasicDBObject query = new BasicDBObject();
		query.put("status", "false");

		cursor = crawldata.find(query);
		cursor.addOption(com.mongodb.Bytes.QUERYOPTION_NOTIMEOUT);
		cursorSize = cursor.size();

		if (cursorSize >= 100) {
			System.out.println("Adding " + cursorSize + " data in list..... Please wait for some time");
			while (cursor.hasNext()) {
				object = cursor.next();

				id = Integer.parseInt(object.get("_id").toString());
				link = object.get("link").toString();

				authorityStatus = object.get("authorityStatus").toString();
				nerStatus = object.get("nerStatus").toString();
				imageStatus = object.get("imageStatus").toString();
				subCategory = object.get("subCategory").toString();
				systemDate = object.get("systemDate").toString();
				moduleName = object.get("moduleName").toString();
				// String translationStatus =
				// object.get("translationStatus").toString();
				translationStatus = "true";

				status = false;

				if (nerStatus.contains("true") && translationStatus.contains("true")) {
					status = true;
				} else {
					status = false;
				}

				if (authorityStatus.contains("false")) {
					data = new localData(id, link, status, subCategory, systemDate, moduleName);
					crawlList.add(data);
				}
			}
			cursor.close();
		}

		if (crawlList.size() <= 100) {
			System.out.println("Data  is less than 100 to get keywords");
			System.out.println("Wait for 2 minutes to get 100 data to fetch keywords");

			System.gc();
			Thread.sleep(120000); // 2 minutes in milliseconds
			main(args);
		} else {
			while (cursorLimit != crawlList.size()) {
				if (batch < 100) {
					list.add(crawlList.get(cursorLimit));
					cursorLimit++;
					batch++;
				} else {
					System.out.println("List is 100 and calling threads");
					new Crawler().crawlSeeds(list);
					System.out.println("Now list is clear");
					list.clear();
					batch = 0;
				}
			}
		}

		if (list.size() > 0) {
			System.out.println("List is " + batch + " and calling threads");
			new Crawler().crawlSeeds(list);
			System.out.println("Final list is clear");
			list.clear();
		}

		long endTime = System.currentTimeMillis();

		System.out.println("Done with all the execution");
		long time = endTime - startTime;
		System.out.println("Program took " + time + " to execute");
		System.out.println("After 2 minutes program will restart");
		System.gc();
		Thread.sleep(120000); // 2 minutes in milliseconds
		main(args);

	}

	public static class MyRunnable implements Runnable {
		private final String urlToProcess, subCategory, systemdate, moduleName;
		private final boolean status;
		List<String> textsToProcess = new ArrayList<String>();
		Document doc;
		Node node1;
		KeywordAutomation keyauto;
		String metaDescription, meatakeywords, title, statusVal;
		BasicDBObject searchQuery;
		BasicDBObject updateFields;
		BasicDBObject setQuery;
		int id;

		MyRunnable(int id,String urlToProcess, boolean status, String subCategory, String systemdate, String moduleName) {
			this.urlToProcess = urlToProcess;
			this.status = status;
			this.subCategory = subCategory;
			this.systemdate = systemdate;
			this.moduleName = moduleName;
			this.id = id;
		}

		@Override
		public void run() {
			node1 = new Node();
			keyauto = new KeywordAutomation();
			try {
				node1 = keyauto.chechMetaData(urlToProcess);
				title = node1.title;
				metaDescription = node1.descriptionContents;
				meatakeywords = node1.keywordContent;
				metaDescription = metaDescription.trim();

				if (!metaDescription.isEmpty()) {

					metaDescription = RemoveSpecialChar.removeSpecialChars(metaDescription);
					searchQuery = new BasicDBObject("link", urlToProcess);
					if (status == false) {
						statusVal = "false";

					} else {
						statusVal = "true";
					}
					updateDate(title, metaDescription, meatakeywords, statusVal, "true");

					if (statusVal == "true") {
						insertObject.append("_id", id);
						insertObject.append("link", urlToProcess);
						insertObject.append("subCategory", subCategory);
						insertObject.append("systemDate", systemdate);
						insertObject.append("moduleName", moduleName);
						dataCountDB.insert(insertObject);
						insertObject.clear();
					}
					System.out.println("Done updating " + urlToProcess);
				}
			}

			catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println("Cannot connect to " + urlToProcess);
				e.printStackTrace();
			}
			// System.out.println("List of text To Process:"+textsToProcess);
		}

		public void updateDate(String title, String description, String keyword, String status,
				String authorityStatus) {
			updateFields = new BasicDBObject();
			updateFields.append("description", description);
			updateFields.append("keyword", keyword);
			updateFields.append("authorityTitle", title);
			updateFields.append("status", status);
			updateFields.append("authorityStatus", authorityStatus);
			setQuery = new BasicDBObject();
			setQuery.append("$set", updateFields);
			samplecrawl.update(searchQuery, setQuery);
			crawldata.update(searchQuery, setQuery);
			setQuery.clear();
		}

	}

}

class localData {
	String url, subCategory, systemDate, moduleName;
	public boolean status;
	int id;
	public localData(int id, String url, boolean status2, String subCategory, String systemDate, String moduleName) {
		super();
		this.url = url;
		this.status = status2;
		this.subCategory = subCategory;
		this.systemDate = systemDate;
		this.moduleName = moduleName;
		this.id = id;
	}
}
