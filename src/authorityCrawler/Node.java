package authorityCrawler;

import java.util.ArrayList;
import java.util.List;

public class Node
{
	List<String> keywordTag=new ArrayList<String>();
	List<String> descriptionTag=new ArrayList<String>();
	String keywordContent="";
	String descriptionContents="";
	String title;
	
	@Override
	public String toString() 
	{
		return "Node [keywordTag=" + keywordTag + ", descriptionTag=" + descriptionTag + ", keywordContent="
				+ keywordContent + ", descriptionContents=" + descriptionContents + "]";
	}
	
	

}
