package mongoDBConnection;

import org.bson.Document;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class GetConnection {

	private static final MongoClient mongoClient = new MongoClient("178.32.49.5", 27017);
	
	private GetConnection(){}
	
	public static DBCollection connect(String collectionName)
	{
		DB db = null;
		DBCollection coll=null;
		try{			
	         // To connect to mongodb server
	         
			 //db=mongoClient.getDB("crawler");
	         db=getConnection().getDB("crawler");
	         coll = db.getCollection(collectionName);
	      }catch(Exception e){
	         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      }
		return coll;
	}
	
	public static MongoClient getConnection(){
        return mongoClient;
    }
	
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		mongoClient.close();
		super.finalize();
	}
	
	public static void main(String[] args) {
		DBCollection coll=GetConnection.connect("domain");
		System.out.println("Connection established!!!");
		System.out.println(coll.count());
		coll=GetConnection.connect("domain");
		System.out.println("Connection established!!!");
		System.out.println(coll.count());
		coll=GetConnection.connect("domain");
		System.out.println("Connection established!!!");
		System.out.println(coll.count());
		coll=GetConnection.connect("domain");
		System.out.println("Connection established!!!");
		System.out.println(coll.count());
		coll=GetConnection.connect("domain");
		System.out.println("Connection established!!!");
		System.out.println(coll.count());
		coll=GetConnection.connect("domain");
		System.out.println("Connection established!!!");
		System.out.println(coll.count());
		
	}
}
